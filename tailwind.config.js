/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./*.{html,js}'],
  theme: {
    screens:{
      '3xl':{'max' : '2560px'},
      '2xl': {'max': '1535px'},
      // => @media (max-width: 1535px) { ... }

      'xl': {'max': '1279px'},
      // => @media (max-width: 1279px) { ... }

      'lg': {'max': '1023px'},
      // => @media (max-width: 1023px) { ... }

      'md': {'max': '767px'},
      // => @media (max-width: 767px) { ... }

      'sm': {'max': '639px'},
      // => @media (max-width: 639px) { ... }
    },
    extend: {
      colors:{
        backGray:'rgba(0,0,0,0.888)',
        emeraldGreen:'#03a06b',
        khakiGreen:'#048b8c'
      },
      backgroundImage:{
        'imageBackground':"url('https://ihub.global/site/wp-content/uploads/2021/05/bg-back-map-jpg.jpg')"
      },
    },
  },
  plugins: [],
}
